# Auto Flight Control HD Clicker

Auto click through the game 'Flight Control HD'. This auto clicker script will let the game play, then loose and start again. This is for the *Veteran* achievement.

## Requirements

- Windows operating system
- [AutoIt v3](https://www.autoitscript.com/site/) installed

## How to use

1. Run this clicker application script `"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" Auto-Flight-Control-Clicker.au3`.
2. Open 'Flight Control HD' in full screen from Steam, start the second level.
3. Close the dailog that confirms that the script started, the clicker script will start clicking through the game.
4. Press the 'Esc' key to stop the script's execution earlier than the specified amount of plays in the script.

## Flight Control HD play count

Search for '*Number of Games Played:*' in the log file:

`C:\Program Files (x86)\Steam\steamapps\common\Flight_Control_HD\output_log.txt`

The count is only logged each time the game starts. So you will need to exit and start the game to get the correct count.

## Tools used for Development

- Click automation developed with the [AutoIt3][autoit] scripting language.

[autoit]: https://www.autoitscript.com/site/
