#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Quintin Henn

 Script Function:
	Automate the game Flight Control HD.

#ce ----------------------------------------------------------------------------

#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <Misc.au3>

Const $appName = "Auto Flight Control HD Clicker"

Global $escapeKeyCode = "1B"
Global $hDLL = DllOpen("user32.dll")

Const $playTimes = 49 ;~ starts at 0 (49+1=50 times)
Local $escPressed = False

MsgBox($MB_ICONINFORMATION, $appName, "Starting. Waiting for Flight Control HD to become active...")

Local $hWnd = WinWaitActive("[TITLE:Flight Control HD; CLASS:FlightControlHDInstance;]")

Sleep(1500)

For $playCount = 0 To $playTimes Step 1

  MouseClick($MOUSE_CLICK_PRIMARY, 100, 1000, 2, 0)

  Local $i = 0
  Do ;~ 20 seconds
    Sleep(100)
    $i = $i + 1

    If _IsPressed($escapeKeyCode, $hDLL) Then
      MsgBox($MB_SYSTEMMODAL, $appName, "The Esc Key was pressed, therefore we will close the application.")
      $escPressed = True
      ExitLoop
    EndIf

  Until $i = 200

  If $escPressed Then ExitLoop

  MouseClick($MOUSE_CLICK_PRIMARY, 900, 600, 1, 0)
  Sleep(300)
  MouseClick($MOUSE_CLICK_PRIMARY, 900, 600, 1, 0)

Next

MsgBox($MB_SYSTEMMODAL, $appName, "You have played " & $playCount & " times!")
